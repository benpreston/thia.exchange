var express = require('express');
var upload = require('multer')({ dest: 'uploads/' });
var router = express.Router();
var ad = require('../modules/autodesk.js');
var fs = require('fs');


var uploads = upload.fields([ {name: 'bundle', maxCount: 1 }, {name:'entryRvt', maxCount: 1 }]);


router.post('/new/bundle', 
			uploads, 
			ad.init, 
			ad.auth, 
			ad.registerBundle,
			ad.uploadBundle,
			ad.assignBundleAlias,
			ad.createActivity,
			ad.uploadInputFile,
			ad.createDownloadUrl,
			ad.createUploadUrl,
			function(req, res, next){
				console.log("Last part triggered");

				try
				{
					let data = JSON.stringify(req.autodesk);
					fs.appendFile('./log/log.txt', data);
				}
				catch(err)
				{
					console.log(err);
				}

				res.status(200);
				res.send("Complete");
			}
		);





router.post('/update/activity', ad.init, ad.auth, ad.updateActivity, function(req, res, next){

	res.json({response: req.autodesk.work.response});

})


router.post('/', function(req, res, next) {

	res.status(400)

});



module.exports = router;