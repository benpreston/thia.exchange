var express = require('express');
var router = express.Router();
var ad = require('../modules/autodesk.js');

var workItem = require('../models/workItem').workItem;
var uuid = require('uuid').v4;
var upload = require('multer')();
var nodeMailer = require('nodemailer');


const addToDB = (req, res, next) => {

	try{
		var uid = uuid();

		var instructionString = req.files.instruction[0].buffer.toString('utf-8');

		var newDoc = {
			uid: uid,
			email: req.body.email,
			instruction: instructionString
		};

		workItem.create(newDoc);

		req.workItem = newDoc;

		next();
	}
	catch(err)
	{
		console.log(err);
		next();
	}
}


const updateDB = (req, res, next) => {
	try 
	{
		workItem.updateOne(
			{uid: req.workItem.uid}, 
			{bimDownloadLink: req.autodesk.upload_url}, 
			{multi:true}, 
			(err, item) => {
				if (err)
					console.log(err);
				if (item)
					console.log(item);

				next();
			});
	}
	catch (err)
	{
		console.log(err);
		next();
	}
}




const bodyFields = 
	upload.fields([
			{name: 'email'}, 
			{name:'instruction'}
		]);



router.post('/new/work', 
	bodyFields, 
	addToDB,
	ad.init,
	ad.auth,
	ad.createDownloadUrl,
	ad.createUploadUrl,
	updateDB,
	ad.work,
	function(req, res, next){

		res.status(200);
		res.json(req.autodesk.work.response);

	});


router.post('/workitem/completed/:wid', function(req, res, next) {

	workItem.findOne({ uid: req.params.wid }, (err, item) => {
		if (err)
		{
			console.log(err);
			res.json({err: err});
		}
		else 
		{
			try{

				let user = process.env.THIA_BMAIL_USER;
				let pass = process.env.THIA_BMAIL_PASS;

				let transporter = nodeMailer.createTransport({
					host: 'smtp.gmail.com',
					port: 465,
					secure: true,
					auth: { user: user, pass: pass }
				});

				transporter.verify().then(console.log).catch(console.error);

				let mailOptions = {
					from: process.env.THIA_BMAIL_USER,
					to: item.email,
					subject: 'BIM Created!',
					text: `Your BIM model has been created! Download it here: ${item.bimDownloadLink}`
				};

				transporter.sendMail(mailOptions, (error, info) => {
					if (error){
						console.log(error);
						res.json({err: err});
					}

					if (info)
					{
						console.log("Message sent: ");
						console.log(info.messageId);
						console.log(info.response);
					}
				})
			}

			catch (err)
			{
				console.log(err);
				res.json({err: err});
			}
		}

		res.send("OK");
	});

});


router.get('/workitem/:wid', function(req, res, next){
	workItem.findOne({ uid: req.params.wid }, (err, item) => {
		if (err)
		{
			console.log(err);
			res.status(404);
			res.json(err);
		}
		res.json(item);
	})
})


router.use('/', function(req, res, next) {

	res.status(200);
	res.send("OK");

})


// router.post('/', ad.init, ad.auth, ad.createDownloadUrl, ad.createUploadUrl, ad.work, function(req, res, next) {
// 	res.json(req.autodesk);
// });


module.exports = router;