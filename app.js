var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');

var s2bRouter = require('./routes/s2b');
var adminRouter = require('./routes/admin');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


mongoose
.connect(`mongodb+srv://${process.env.MONGO_DEV_USER}:${process.env.MONGO_DEV_PASS}@cluster0.9ol0e.mongodb.net/pocDb?ssl=true&authSource=admin&w=majority`)
.then(
  () => console.log("MongoDB connected..."),
  err => console.log(err.message)
);


app.use('/s2b', s2bRouter);

app.use('/admin', adminRouter);

app.use('/public', express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

var upload = require('multer')({dest: 'uploads/'});


var uploads = upload.fields([ 
  {name: 'name', maxCount: 1}, 
  {name: 'email', maxCount: 1}, 
  {name: 'file', maxCount: 1}]);


app.post('/test', uploads, function(req,res,next) {
  console.log(req);
  res.status(204);
  res.send();
})


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({error: err})
});


module.exports = app;
