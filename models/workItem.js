var mongoose = require("mongoose");

var workItemSchema = new mongoose.Schema({

	uid: {
		type: String,
		index: true
	},
	email: {
		type: String,
	},
	instruction: {
		type: String
	},
	bimDownloadLink: {
		type: String
	}

});

module.exports.workItem = mongoose.model('workItem', workItemSchema);