function mainForm() {

	let fd = {
		formData: {
			name:'',
			email: '',
			file: null,
			fileName: ''
		},
		message: '',

		onFileChange() {
			var file = document.getElementById("thFormFile");
			this.formData.fileName = file.files[0].name;
		},

		submitData() {
			this.message = '';

			var file = document.getElementById("thFormFile");

			var formData = new FormData();

			//formData.append("name", this.formData.name);
			formData.append("email", this.formData.email);
			formData.append("instruction", file.files[0]);

			axios({
				method:'post',
				url:'s2b/new/work',
				data: formData,
				headers: {"Content-Type": "multipart/form-data"}

			}) 
			.then(() => {
				this.message = 'Request submitted.'
			})
			.catch(() => {
				this.message = 'Something went wrong...'
			});
		}
	}

	return fd;
}