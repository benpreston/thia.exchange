const axios = require('axios').default;
const querystring = require('querystring');
const FormData = require('form-data');
const fs = require('fs');

module.exports.init = (req, res, next) => {
	req.autodesk = {};
	next();
}

module.exports.auth = async (req, res, next) => {

	try
	{
		let headers = {
			'Content-Type': 'application/x-www-form-urlencoded'
		};

		let requestData = querystring.stringify({
			client_id: process.env.AUTOD_POC_CLID,
			client_secret: process.env.AUTOD_POC_CLST,
			grant_type: 'client_credentials',
			scope: 'code:all data:write data:read bucket:create bucket:delete'
		});


		//TODO: we can change the scope of the authentication at a later date if need be.

		const response = await axios({
			method: 'POST',
			url: 'https://developer.api.autodesk.com/authentication/v1/authenticate',
			data: requestData,
			headers: headers
		})

		req.autodesk.access_token = response.data.access_token;
		next();
	}

	catch (err)
	{
		console.log("Error in autodesk.auth");
		console.log(err);
		res.status(401);
		res.json({error: err});
	}
}


module.exports.work = async (req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	try
	{

		//TODO: store bearer token server side so new tokens are not needed every time a work item needs to be submitted

		let headers = {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};

		let requestData = JSON.stringify({
			"activityId": `${process.env.AUTOD_POC_NKNM}.CreateDocumentAppActivity+test`,
			"arguments": {
				"rvtFile": { 
					"url": req.autodesk.download_url
				},
				"onComplete": {
					verb: "post",
					"url": `https://thialabs-datatransfer.herokuapp.com/s2b/workitem/completed/${req.workItem.uid}`
				},
				"instructionData": {
					"url": `https://thialabs-datatransfer.herokuapp.com/s2b/workitem/${req.workItem.uid}`
				},
				"result": {
					"verb": "put",
					"url": req.autodesk.upload_url
				}
			}});


		//TODO: we can change the scope of the authentication at a later date if need be.

		const response = await axios({
			method: 'POST',
			url: 'https://developer.api.autodesk.com/da/us-east/v3/workitems',
			data: requestData,
			headers: headers
		})

		req.autodesk.work = {};
		req.autodesk.work.response = response.data;

		next();
	}


	catch (err)
	{
		console.log("Error in autodesk.work");
		console.log(req.autodesk);
		console.log(err);
		res.status(500);
		res.json({error: err});
	}
}



module.exports.createDownloadUrl = async (req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	console.log("createDownloadUrl triggered");

	try 
	{

		let headers = {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};

		let requestData = {}


		//TODO: we can change the scope of the authentication at a later date if need be.

		const response = await axios({
			method: 'POST',
			url: `https://developer.api.autodesk.com/oss/v2/buckets/${process.env.AUTOD_POC_BCKT}/objects/${process.env.AUTOD_POC_IKEY}/signed`,
			data: requestData,
			headers: headers
		})

		req.autodesk.download_url = response.data.signedUrl;

		next();
	}
	catch (err)
	{
		console.log("Error in autodesk.createDownloadUrl");
		res.status(500);
		res.json(err);
	}
}


module.exports.createUploadUrl = async (req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	console.log("createUploadUrl triggered");

	try 
	{

		let headers = {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};

		let requestData = {}


		//TODO: we can change the scope of the authentication at a later date if need be.

		const response = await axios({
			method: 'POST',
			url: `https://developer.api.autodesk.com/oss/v2/buckets/${process.env.AUTOD_POC_BCKT}/objects/${process.env.AUTOD_POC_OKEY}/signed?access=readwrite`,
			data: requestData,
			headers: headers
		})

		req.autodesk.upload_url = response.data.signedUrl;

		next();
	}
	catch (err)
	{
		console.log("Error in autodesk.createUploadUrl");
		res.status(500);
		res.json(err);
	}
}


module.exports.uploadBundle = async(req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	try
	{
		let formData = new FormData();

		// Add all keys given to us in the response of RegisterBundle
		for (const [key, value] of Object.entries(req.autodesk.appBundle.uploadParameters.formData))
		{
			formData.append(`${key}`, `${value}`);
		}

		const file = fs.createReadStream(`./${req.files.bundle[0].path}`);


		// Attach the user-uploaded file. Handled by Multer
		formData.append('file', file);

		// for (var pair of formData.entries()) {
		//     console.log(pair[0]+ ', ' + pair[1]); 
		// }

		formData.submit(req.autodesk.appBundle.uploadParameters.endpointURL, function(err, fRes) {

			if (err)
				throw(err);

			req.autodesk.appBundle.uploadResponse = fRes;
			next();
		})

	}
	catch(err)
	{
		console.log("Error in autodesk.uploadBundle");
		console.log(err);
		res.status(500);
		res.json({error: err});
	}
}



module.exports.createActivity = async(req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	try 
	{
		let headers = {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};

		let requestData = {
            "id": `${req.body.id}Activity`,
            "commandLine": [ `$(engine.path)\\\\revitcoreconsole.exe /i \"$(args[rvtFile].path)\" /al \"$(appbundles[${req.body.id}].path)\"` ],
            "parameters": {
              "rvtFile": {
                "zip": false,
                "ondemand": false,
                "verb": "get",
                "description": "Input Revit model",
                "required": true,
                "localName": "$(rvtFile)"
              },

              "instructionData": {
              	"zip": false,
              	"ondemand": true,
              	"verb": "get",
              	"description": "Additional data needed for runtime",
              	"required": true
              },

              "result": {
                "zip": false,
                "ondemand": false,
                "verb": "put",
                "description": "Results",
                "required": true,
                "localName": "result.rvt"
              }
            },
            "engine": `Autodesk.Revit+${req.body.version}`,
            "appbundles": [ `${process.env.AUTOD_POC_NKNM}.${req.body.id}+test` ],
            "description": `${req.body.description}`
    	};

    	//TODO: hardcoded app alias above needs to be removed


    	axios({
    		method:"POST",
    		url: "https://developer.api.autodesk.com/da/us-east/v3/activities",
    		data: requestData,
    		headers: headers
    	}).then((response) => {

			req.autodesk.activityResponse = response.data;

			axios({
				method: 'POST',
				url: `https://developer.api.autodesk.com/da/us-east/v3/activities/${req.body.id}Activity/aliases`,
				data: {
					version: req.autodesk.activityResponse.version,
					id: "test"
				},
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${req.autodesk.access_token}`
				}
			})
			.then((aliasResponse) => {
				req.autodesk.activityAliasResponse = aliasResponse.data;
				next();
			})
    	})

	}

	catch(err)
	{
		console.log("Error in autodesk.createActivity");
		console.log(err);
		res.status(500);
		res.json({error: err});
	}

}



module.exports.uploadInputFile = async(req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	try
	{
		let headers = {
			'accept-encoding': 'gzip, deflate',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};


		let response = await axios({
			method: 'PUT',
			url: `https://developer.api.autodesk.com/oss/v2/buckets/${process.env.AUTOD_POC_BCKT}/objects/${process.env.AUTOD_POC_IKEY}`,
			data: fs.createReadStream(req.files.entryRvt[0].path),
			headers: headers
		});


		if (response)
		{
			console.log("Uploaded RVT file:");
			console.log(response.data);
		}

		req.autodesk.inputRVTResponse = response.data;

		next();

	}
	catch(err)
	{
		console.log("Error in autodesk.uploadInputFile");
		console.log(err);
		res.status(500);
		res.json(err);
	}
}



module.exports.createBucket = async (req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	try
	{
		let headers = {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};


		// TODO: update id of alias to not be hardcoded
		let requestData = {
            "bucketKey": `${process.env.AUTOD_POC_BCKT}`,
            "access": "full",
            "policyKey": "transient"
        };


		let response = await axios({
			method: 'POST',
			url: `https://developer.api.autodesk.com/oss/v2/buckets`,
			data: requestData,
			headers: headers
		});


		if (response)
		{
			console.log("Created bucket:");
			console.log(response.data);
		}

		req.autodesk.bucketResponse = response.data;

		next();

	}
	catch(err)
	{
		console.log("Error in autodesk.createBucket");
		console.log(err);
		res.status(500);
		res.json({error: err});
	}
}



module.exports.assignBundleAlias = async(req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	try
	{
		let headers = {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};


		// TODO: update id of alias to not be hardcoded
		let requestData = {
			version: req.autodesk.appBundle.version,
			id: "test"
		};


		let response = await axios({
			method: 'POST',
			url: `https://developer.api.autodesk.com/da/us-east/v3/appbundles/${req.body.id}/aliases`,
			data: requestData,
			headers: headers
		});


		if (response)
		{
			console.log("Created alias:");
			console.log(response.data);
		}

		req.autodesk.bundleAliasResponse = response;

		next();

	}
	catch(err)
	{
		console.log("Error in autodesk.assignBundleAlias");
		res.status(500);
		res.json({error: err});
	}

}


module.exports.registerBundle = async(req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	try
	{
		let headers = {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};


		let requestData = {
			id: req.body.id,
			engine: `Autodesk.Revit+${req.body.version}`,
			description: req.body.description
		};


		let response = await axios({
			method: 'POST',
			url: 'https://developer.api.autodesk.com/da/us-east/v3/appbundles',
			data: requestData,
			headers: headers
		});


		if (response)
		{
			console.log("Registered new bundle:");
			console.log(response.data);
		}

		req.autodesk.appBundle = {}
		req.autodesk.appBundle.uploadParameters = response.data.uploadParameters;
		req.autodesk.appBundle.version = response.data.version;

		next();

	}
	catch(err)
	{
		console.log("Error in autodesk.registerBundle");
		res.status(500);
		res.json({error: err});
	}
}


module.exports.updateActivity = async(req, res, next) => {

	if (!req.autodesk) res.status(500);
	if (!req.autodesk.access_token) res.status(401);

	try
	{

		let headers = {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${req.autodesk.access_token}`
		};

		const requestData = {
			"id": null,
			"commandLine": [ "$(engine.path)\\\\revitcoreconsole.exe /i \"$(args[rvtFile].path)\" /al \"$(appbundles[ThiaPOCApp].path)\"" ],
			"parameters": {
				"rvtFile": {
					"verb": "get",
					"zip":false,
					"ondemand":false,
					"description": "Input Revit model",
					"required": true,
					"localName": "input.rvt"
				},
				"result": {
					"zip": false,
					"ondemand": false,
					"verb": "put",
					"description": "Results",
					"required": true,
					"localName": "result.rvt"
				}
			},
			"engine": "Autodesk.Revit+2021",
			"appbundles": [ `${process.env.AUTOD_POC_NKNM}.ThiaPOCApp+pocApp` ],
			"description": "Updated description."
		}


		const response = await axios({
			method: 'POST',
			url: `https://developer.api.autodesk.com/da/us-east/v3/activities/POCTestActivity/versions`,
			data: requestData,
			headers: headers
		});


		console.log("Updated activity. Version: " + response.data.version);

		const aliasResponse = await axios({
			method: 'PATCH',
			url: 'https://developer.api.autodesk.com/da/us-east/v3/activities/POCTestActivity/aliases/test',
			headers: headers,
			data: {
				"version": response.data.version
			}
		});

		req.autodesk.work = {}
		req.autodesk.work.response = response;

		next();

	}
	catch (err)
	{
		console.log("Error in autodesk.updateActivity");
		res.status(500);
		res.json({error: err});
	}

}